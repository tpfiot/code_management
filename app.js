const express = require('express');
const  path =  require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
var passport = require('passport');
const session = require('express-session')
const expressMessages = require('express-messages')
var Product = require('./models/products')
var config = require('./config/database');
const https = require('https');
const fs = require('fs');

//ssl certificate
//const privateKey = fs.readFileSync('/home/ubuntu/https_server/privkey.pem', 'utf8');
//const certificate = fs.readFileSync('/home/ubuntu/https_server/cert.pem', 'utf8');
//const ca = fs.readFileSync('/home/ubuntu/https_server/chain.pem', 'utf8');

const privateKey = fs.readFileSync('/home/ubuntu/http_server/key.pem', 'utf8');
const certificate = fs.readFileSync('/home/ubuntu/http_server/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/ssl/certs/chain.pem', 'utf8');

const credentials = {
      key: privateKey,
        cert: certificate,
        ca: ca,
        passphrase:'test1234'
};


mongoose.connect(config.database,{useNewUrlParser:true, useUnifiedTopology: true});

var db = mongoose.connection;

db.once('open',function(){
console.log("********Connected to MongoDB******")
})

db.on('error',function(err){
    console.log(err)
})

const port = 7000;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname,'public')))

app.set('views',path.join(__dirname,'views'))
app.set('view engine','pug')

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}))


app.use(flash());
app.use((req, res, next) => {
  res.locals.errors = req.flash("error");
  res.locals.successes = req.flash("success");
  next();
});

  app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.')
        , root    = namespace.shift()
        , formParam = root;
  
      while(namespace.length) {
        formParam += '[' + namespace.shift() + ']';
      }
      return {
        param : formParam,
        msg   : msg,
        value : value
      };
    }
  }));


require('./config/passport')(passport)
app.use(passport.initialize());
app.use(passport.session());

app.get('*',function(req,res,next){
  res.locals.user = req.user || null;
  next()
})

var products = require('./routes/products');
var users = require('./routes/users');
app.use('/products',products)
app.use('/users',users)

app.get('/',function(req,res){
  
      Product.find({},function(err,products){
        if(err){
            console.log(err)
        }
        else{
            res.render('index',{title : "Product List",products : products});
        }
        
    })
})



/*app.listen(port,function(){
console.log("Server started....in port: "+port)

})*/

const httpsServer = https.createServer(credentials, app);

httpsServer.listen(port, () => {
    console.log('HTTPS Server running on port..'+port);
});
