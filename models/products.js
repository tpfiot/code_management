var mongoose = require('mongoose');

var productsSchema = mongoose.Schema({
name :{
type : String,
require : true
  },
price : {
      type:String,
      require: true
  },
os:{
type : String,
require : true
},
model : {
    type:String,
    require : true
},
spec : {
    type:String,
    require : true
}

})

let product = module.exports = mongoose.model('product',productsSchema)