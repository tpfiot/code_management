const express = require('express');
const router = express.Router();
var Product = require('../models/products')

router.get('/',ensureAuthenticated,function(req,res){
    
    Product.find({},function(err,products){
        if(err){
            console.log(err)
        }
        else{
            res.render('products_insert',{title : "Enter mobile details:",products : products});
        }
        
    })
})

router.get('/:id',function(req,res){
    Product.findById(req.params.id,function(err,product){
        if(err){
            console.log(err)
        }
        else{
            console.log(product)
           res.render('product_single',{title : "Mobile Details ",product : product});
        }
    })

})


router.get('/edit/:id',ensureAuthenticated,function(req,res){
    Product.findById(req.params.id,function(err,product){
        if(err){
            console.log(err)
        }
        else{
            console.log(product)
           res.render('product_edit',{product : product});
        }
    })

})

router.post('/',function(req,res){
  
  req.checkBody('name','Title is required').notEmpty()
  req.checkBody('price','Price is required').notEmpty()
  req.checkBody('model','Model is required').notEmpty()
  req.checkBody('os','os is required').notEmpty()
  req.checkBody('spec','spec is required').notEmpty()

  var errors = req.validationErrors();
  
  if(errors){
    res.render('products_insert',{
        title:'Add Products',
        errors:errors
    })
  }else{

    var products = new Product();
    products.name = req.body.name;
    products.price = req.body.price;
    products.model = req.body.model;
    products.os = req.body.os;
    products.spec = req.body.spec;
    products.save(function(err){
     if(err){
         console.log(err)
         return;
     }
     else{
       // res.flash('success','Product added')
      // res.flash('success','Product added');
        req.flash("success", "Product added");
        res.redirect('/')
     }
  
    })

  }


 
})

router.post('/edit/:id',function(req,res){

    req.checkBody('name','Title is required').notEmpty()
    req.checkBody('price','Price is required').notEmpty()
    req.checkBody('model','Model is required').notEmpty()
    req.checkBody('os','os is required').notEmpty()
    req.checkBody('spec','spec is required').notEmpty()
    
    var products = {};
    products.name = req.body.name;
    products.price = req.body.price;
    products.model = req.body.model;
    products.os = req.body.os;
    products.spec = req.body.spec;

    var query = {_id:req.params.id};

    Product.updateOne(query,products,function(err){
     if(err){
         console.log(err)
         return;
     }
     else{
       // res.flash()
        req.flash('success','Update success');
        res.redirect('/')
     }
  
    })
   
  })

router.delete('/:id',function(req,res){
var query = {_id:req.params.id}
Product.deleteOne(query,function(err){
if(err){
    console.log(err)
}
res.send('Success');
})

})

function ensureAuthenticated(req, res, next){
    if(req.isAuthenticated()){
      return next();
    } else {
      req.flash('danger', 'Please login');
      res.redirect('/users/login');
    }
  }

  
module.exports = router;